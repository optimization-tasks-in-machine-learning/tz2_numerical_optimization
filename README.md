# ТЗ2 Методы одномерной оптимизации

Подготовлено студентами Финансового университета при Правительстве Российской Федерации по дисциплине Оптимизационные задачи в машинном обучении. Для ознакомления с поставленной задачей нажмите [читать tz2_numerical_optimization.pdf](illustrations/tz2_numerical_optimization.pdf)

## Демонстрация возможностей

Множество примеров работы модуля доступны к просмотру любым удобным для Вас способом. <br/>
* Онлайн Google Colab Notebook - нажмите [открыть Google Colab](https://colab.research.google.com/drive/1PSOY9WQq-JqAgW8bM2NJx1v6F1IhvDSB?usp=sharing)
* Оффлайн Jupyter Notebook - нажмите [скачать tz2_numerical_optimization.ipynb](tz2_numerical_optimization.ipynb)

## Иллюстрация работы

* Решение и вывод
![Solution of problem](illustrations/conclusion_and_solution.png) <br/>
* Простая визуализация <br/>
![Interactive visualization](illustrations/simple_visualization.gif) <br/>
* Продвинутая визуализация <br/>
![Interactive visualization](illustrations/pro_visualization.gif) <br/>

## Подготовка окружения

```
# Для визуализации необходим CUDA
!nvidia-smi
!pip install GPUtil
```

## Импорт модуля

```
# Клонирование репозитория
!git clone https://leonidalekseev:maCRxpzWvEcohHKKTeG9@gitlab.com/optimization-tasks-in-machine-learning/tz2_numerical_optimization.git
# Импорт всех функций из модуля
from tz2_numerical_optimization.utils import *
```

## Документация функций

`help(set_function)`

```
Help on function set_function in module tz2_numerical_optimization.utils:

set_function(input_investigated_function: str, functions_symbols: Union[tuple, NoneType] = None, functions_title: str = 'Введенная \\space функция:', functions_designation: str = 'F', is_display_input: bool = True, _is_system: bool = False) -> tuple
    Установка объекта sympy функции из строки.
    Количество переменных проверяется. Отображение функции настраивается.
    
    Parameters
    ===========
    
    input_investigated_function: str
        Входная строка с функцией
    functions_symbols: tuple, optional
        Переменные функции
    functions_title: str
        Заголовок для отображения
    functions_designation: str
        Обозначение для отображения
    is_display_input: bool
        Отображать входную функцию или нет
    _is_system: bool
        Вызов функции программой или нет
    
    Returns
    ===========
    
    input_investigated_function: str
        Входная строка с функцией
    investigated_function: sympy
        Исследуемая функция
    functions_symbols: tuple
        Переменные функции
```

`help(visualize_plotly)`

```
Help on function visualize_plotly in module tz2_numerical_optimization.utils:

visualize_plotly(input_investigated_function: str, functions_symbols: Union[tuple, NoneType] = None, input_bound_function: Union[str, NoneType] = None, parabols_functions: tuple = (), extremum_points: tuple = (), is_display_input: bool = True, start: Union[int, float] = -1, stop: Union[int, float] = 1, detail: Union[int, float] = 100) -> None
    Визуализация функции с оптимизацией численном методом парабол. 
    
    Parameters
    ===========
    
    input_investigated_function: str
        Исследуемая функция
    functions_symbols: tuple, optional
        Переменные функции
    input_bound_function: str, optional
        Ограничивающая функция
    parabols_functions: tuple, optional
        Параболы оптимизации
    extremum_points: tuple, optional
        Точки экстремума
    is_display_input: bool
        Отображать входную функцию или нет
    start: int, float
        Начало графика
    stop: int, float
        Конец графика
    detail: int, float
        Детализация графика
```

`help(find_extremums)`

```
Help on function numerical_optimization in module tz2_numerical_optimization.utils:

numerical_optimization(method: Union[int, str], input_investigated_function: str, bound_function: Union[tuple, NoneType] = None, start_point: Union[tuple, NoneType] = None, accuracy: float = 1e-05, max_iterations: int = 500, is_display_input: bool = True, is_display_solution: bool = True, is_save_solution: bool = False, is_display_conclusion: bool = True, is_try_visualize: bool = False) -> tuple
    Численная птимизация одномерной функции с заданными параметрами. 
    
    Parameters
    ===========
    
    method: int, str
        Численные методы одномерной оптимизации
        0, 'golden section': метод золотого сечения
        1, 'parabola': метод парабол
        2, 'brent': метод Брента
        3, 'bfgs': метод БФГС
    input_investigated_function: str
        Входная строка с функцией
    bound_function: tuple, optional
        Начальная точка в методах 0, 1, 2
    start_point: tuple, optional
        Начальная точка в методе 3
    accuracy: float
        Точность во всех методах
    max_iterations: int
        Максимум итераций во всех методах
    is_display_input: bool = True
        Отображать входную функцию или нет
    is_display_solution: bool = True
        Отображать решение или нет
    is_save_solution: bool = False
        Сохранять решение или нет
    is_display_conclusion: bool = True
        Отображать вывод или нет               
    is_try_visualize: bool = False
        Визуализация процесса в методе 1
    
    Returns
    ===========
    
    function_point: numpy.float64
        Найденая точка экстремума
    function_value: numpy.float64
        Значение функции в точке экстремума
    flag_process: int
        Флаг завершения алгоритма
        0: найдено значение с заданной точностью
        1: достигнуто максимальное количество итераций
        2: выполнено с ошибкой
```

## Участники проекта

* [Быханов Никита](https://gitlab.com/BNik2001) - Менеджер проектa, Тестировщик
* [Алексеев Леонид](https://gitlab.com/LeonidAlekseev) - Программист, Тестировщик
* [Семёнова Полина](https://gitlab.com/polli_eee) - Аналитик, Тестировщик
* [Янина Марина](https://gitlab.com/marinatdd) - Аналитик, Тестировщик
* [Буркина Елизавета](https://gitlab.com/lizaburkina) - Аналитик
* [Егорин Никита](https://gitlab.com/hadesm8) - Аналитик

`Группа ПМ19-1`

## Используемые источники

* [Sympy documentation](https://www.sympy.org/en/index.html)
* [Plotly documentation](https://plotly.com/python/)
